/*
 * File:   ServerApp.h
 * Author: tiennd
 *
 * Created on November 7, 2014, 4:16 PM
 */

#ifndef SERVERAPP_H
#define	SERVERAPP_H


#include "Poco/Util/ServerApplication.h"
#include <vector>
#include <iostream>
#include "ServiceHandler.h"

#include "upframework/upbase/base/UPZKService.h"

class ServerApp : public Poco::Util::ServerApplication {
public:
    ServerApp();
    virtual ~ServerApp();

    void defineOptions(Poco::Util::OptionSet& options);
    void handleShowHelp(const std::string& name, const std::string& value);

    void initialize(Poco::Util::Application& app);
    virtual int main(const std::vector<std::string>& args);


private:
    bool _showHelp;
    UPZKServiceRegister _zkReg;
    UPZKServiceRegister _zkMonitorReg;

};

#endif	/* SERVERAPP_H */

