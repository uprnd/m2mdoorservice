#pragma once

#include "ZServiceModel.h"
#include "ServiceThriftHandlerBaseT.h"
#include "M2MDoorService.h"
#include "ServiceFactory.h"

namespace Up {
namespace Core {
namespace M2M {
namespace Door {

class ServiceHandler : public TServiceThriftHandlerBaseT<ZServiceModel,
M2MDoorServiceIf, M2MDoorServiceProcessor,
::apache::thrift::protocol::TCompactProtocolFactory > {
public:
    typedef TServiceThriftHandlerBaseT<ZServiceModel,
    M2MDoorServiceIf, M2MDoorServiceProcessor,
    ::apache::thrift::protocol::TCompactProtocolFactory > _Base;

    ServiceHandler(Poco::SharedPtr<ZServiceModel> aModel) : _Base(aModel) {
    };

public:

    virtual void addDoor(TResult& _return, const TKey& tKey, const TValue& tValue) {
        if (m_pmodel) m_pmodel->addDoor(_return, tKey, tValue);
    }

    virtual void getDoor(TResult& _return, const TKey& tKey) {
        if (m_pmodel) m_pmodel->getDoor(_return, tKey);
    }

    virtual void multiGetDoor(listResult& _return, const TKeyList& tKeyList) {
        if (m_pmodel) m_pmodel->multiGetDoor(_return, tKeyList);
    }

    virtual void putDoor(TResult& _return, const TKey& tKey, const TValue& tValue) {
        if (m_pmodel) m_pmodel->putDoor(_return, tKey, tValue);
    }

    virtual void removeDoor(TResult& _return, const TKey& tKey) {
        if (m_pmodel) m_pmodel->removeDoor(_return, tKey);
    }

    virtual void updateDoor(TResult& _return, const TKey& tKey, const TValue& tValue) {
        if (m_pmodel) m_pmodel->updateDoor(_return, tKey, tValue);
    }

    virtual void listDoor(listResult& _return, const int32_t start, const int32_t limit) {
        if (m_pmodel) m_pmodel->listDoor(_return, start, limit);
    }

};

}
}
}
}
