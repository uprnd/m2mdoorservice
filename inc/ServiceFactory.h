/*
 * File:   ServiceFactory.h
 * Author: tiennd
 *
 * Created on November 7, 2014, 5:37 PM
 */

#ifndef SERVICEFACTORY_H
#define	SERVICEFACTORY_H


#include "storagedef.h"
#include "ServiceFactoryT.h"
#include "ZServiceModel.h"
#include <Base/EndpointManager.h>

namespace Up {
namespace Core {
namespace M2M {
namespace Door {

class ServiceFactory : public ServiceFactoryT< CacheType, CacheFactory, BackendStorageType, PersistentStorageType, ZServiceModel > {
public:
    static void init(Poco::Util::Application& app);

private:
    typedef ServiceFactoryT< CacheType, CacheFactory, BackendStorageType, PersistentStorageType, ZServiceModel > _Base;


};

}
}
}
}

#endif	/* SERVICEFACTORY_H */

