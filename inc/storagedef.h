/*
 * File:   storagedef.h
 * Author: tiennd
 *
 * Created on November 7, 2014, 5:40 PM
 */

#ifndef STORAGEDEF_H
#define	STORAGEDEF_H


#include "ModelData.h"
#include <Storage/CachePersistent.h>
#include <Caching/AbstractCache.h>
#include <Caching/CacheFactory.h>
#include <thriftutil/TSerializer.h>
#include <Hashing/DefaultHasher.h>
#include "m2mdoor_types.h"

#include <iostream>


namespace Up {
namespace Core {
namespace M2M {
namespace Door {


typedef Up::Storage::ObjectStorage< TKey, model::ModelData, TThriftSerializer<TValue> > BackendStorageType;
typedef Up::Storage::CachePersistent<TKey, model::ModelData, BackendStorageType> PersistentStorageType;
typedef PersistentStorageType::CacheType CacheType;
typedef Up::Hashing::DefaultHasher HasherType;
typedef Up::Caching::BasicCacheFactory<TKey, model::ModelData, HasherType> CacheFactory;

}
}
}
}


#endif	/* STORAGEDEF_H */

