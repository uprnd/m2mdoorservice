/*
 * File:   ZServiceModel.h
 * Author: tiennd
 *
 * Created on November 7, 2014, 5:36 PM
 */
#pragma once


#ifndef ZSERVICEMODEL_H
#define	ZSERVICEMODEL_H

#include "storagedef.h"


namespace Up {
namespace Core {
namespace M2M {
namespace Door {

class ZServiceModel {
public:

    static void traceFunction(const std::string& strFunc);
    static void tracePagram(int num, ...);

    ZServiceModel(Poco::SharedPtr<PersistentStorageType> aStorage);
    virtual ~ZServiceModel();

    void addDoor(TResult& _return, const TKey& tKey, const TValue& tValue);
    void getDoor(TResult& _return, const TKey& tKey);
    void putDoor(TResult& _return, const TKey& tKey, const TValue& tValue);
    void removeDoor(TResult& _return, const TKey& tKey);
    void updateDoor(TResult& _return, const TKey& tKey, const TValue& tValue);

    void multiGetDoor(listResult& _return, const TKeyList& tKeyList);
    void listDoor(listResult& _return, const int32_t start, const int32_t limit);

private:
    ZServiceModel(const ZServiceModel& orig);
    Poco::SharedPtr<PersistentStorageType> _storage;

    void deSerializeKey(const TKey& tkey, std::string& uuid, int16_t& major, int16_t& minor) {

        int uuidSize;
        unsigned char *pKey = (unsigned char *) tkey.data();
        memcpy(&uuidSize, pKey, sizeof (uuidSize));

        uuid.resize(uuidSize);
        unsigned char *pUuid = (unsigned char*) uuid.data();
        pKey += sizeof (uuidSize);
        memcpy(pUuid, pKey, uuidSize);

        pKey += uuidSize;
        const int lengthInt16 = 2;
        memcpy(&major, pKey, lengthInt16);

        pKey += lengthInt16;
        memcpy(&minor, pKey, lengthInt16);
    }


};

#define TRACE_FUNCTION ZServiceModel::traceFunction(__FUNCTION__)

}
}
}
}

#endif	/* ZSERVICEMODEL_H */

