#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/src/ModelData.o \
	${OBJECTDIR}/src/ServerApp.o \
	${OBJECTDIR}/src/ServiceFactory.o \
	${OBJECTDIR}/src/ZServiceModel.o \
	${OBJECTDIR}/thrift/gen-cpp/M2MDoorService.o \
	${OBJECTDIR}/thrift/gen-cpp/m2mdoor_constants.o \
	${OBJECTDIR}/thrift/gen-cpp/m2mdoor_types.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-std=c++0x
CXXFLAGS=-std=c++0x

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk bin/m2mdoorservice

bin/m2mdoorservice: ${OBJECTFILES}
	${MKDIR} -p bin
	${LINK.cc} -o bin/m2mdoorservice ${OBJECTFILES} ${LDLIBSOPTIONS} ../corelibs/UPStoreAppCommon/lib/libupstoreappcommon.a ../corelibs/UPStorage/lib/libupstorage.a ../corelibs/UPDistributed/lib/libupdistributed.a ../corelibs/UPCaching/lib/libupcaching.a ../corelibs/UPHashing/lib/libuphashing.a ../corelibs/UPBase/lib/libupbase.a ../corelibs/UPThrift/lib/libupthrift.a ../corelibs/UPPoco/lib/libuppoco.a ../corelibs/UPEvent/lib/libupevent.a ../corelibs/UPMalloc/lib/libupmalloc.a -lpthread -ldl -lrt

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -I../corelibs/UPStoreAppCommon/inc/monitor -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/src/ModelData.o: src/ModelData.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -I../corelibs/UPStoreAppCommon/inc/monitor -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ModelData.o src/ModelData.cpp

${OBJECTDIR}/src/ServerApp.o: src/ServerApp.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -I../corelibs/UPStoreAppCommon/inc/monitor -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ServerApp.o src/ServerApp.cpp

${OBJECTDIR}/src/ServiceFactory.o: src/ServiceFactory.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -I../corelibs/UPStoreAppCommon/inc/monitor -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ServiceFactory.o src/ServiceFactory.cpp

${OBJECTDIR}/src/ZServiceModel.o: src/ZServiceModel.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -I../corelibs/UPStoreAppCommon/inc/monitor -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ZServiceModel.o src/ZServiceModel.cpp

${OBJECTDIR}/thrift/gen-cpp/M2MDoorService.o: thrift/gen-cpp/M2MDoorService.cpp 
	${MKDIR} -p ${OBJECTDIR}/thrift/gen-cpp
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -I../corelibs/UPStoreAppCommon/inc/monitor -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/thrift/gen-cpp/M2MDoorService.o thrift/gen-cpp/M2MDoorService.cpp

${OBJECTDIR}/thrift/gen-cpp/m2mdoor_constants.o: thrift/gen-cpp/m2mdoor_constants.cpp 
	${MKDIR} -p ${OBJECTDIR}/thrift/gen-cpp
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -I../corelibs/UPStoreAppCommon/inc/monitor -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/thrift/gen-cpp/m2mdoor_constants.o thrift/gen-cpp/m2mdoor_constants.cpp

${OBJECTDIR}/thrift/gen-cpp/m2mdoor_types.o: thrift/gen-cpp/m2mdoor_types.cpp 
	${MKDIR} -p ${OBJECTDIR}/thrift/gen-cpp
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -I../corelibs/UPStoreAppCommon/inc/monitor -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/thrift/gen-cpp/m2mdoor_types.o thrift/gen-cpp/m2mdoor_types.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} bin/m2mdoorservice

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
