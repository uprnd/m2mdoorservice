/*
 * File:   ZServiceModel.cpp
 * Author: tiennd
 *
 * Created on November 7, 2014, 5:36 PM
 */

#include "ZServiceModel.h"
#include "ServiceFactory.h"
#include "ExceptionServer.h"
#include <iostream>


namespace Up {
namespace Core {
namespace M2M {
namespace Door {

void ZServiceModel::traceFunction(const std::string& strFunc) {
#ifdef DLOG
	std::cout << strFunc << " is called\n";
#endif
}

void ZServiceModel::tracePagram(int num, ...) {
#ifdef DLOG
	va_list vl;
	va_start(vl, num);
	for (int i = 0; i < num; ++i) {
		std::cout << va_arg(vl, const char *);
	}
	std::cout << "\n";
#endif
}

ZServiceModel::ZServiceModel(Poco::SharedPtr<PersistentStorageType> aStorage)
: _storage(aStorage) {
}

ZServiceModel::ZServiceModel(const ZServiceModel& orig) {

}

ZServiceModel::~ZServiceModel() {

}

class addDoor_visitor : public PersistentStorageType::data_visitor {
public:

	addDoor_visitor(TResult& return_, const Door::TValue& vl)
	: _return(return_), _vl(vl) {
		// FIXME
		TRACE_FUNCTION;
	}

	virtual bool visit(const PersistentStorageType::TKey& key, PersistentStorageType::TValue& value) {
		if (value != model::ModelData::defVal) throw EXCEPTION_SERVER(M2M_DOOR, ErrorCode::ERR_ITEM_EXIST);
		value.copyFrom(_vl);
		_return.errorCode = ErrorCode::ERR_NONE;
		return true;
	}

private:
	TResult& _return;
	const Door::TValue& _vl;
};

void ZServiceModel::addDoor(TResult& _return, const TKey& tKey, const TValue& tValue) {

	try {

		if (_storage.get() == NULL) throw EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_NULL_STORAGE);
		addDoor_visitor visitor(_return, tValue);
		_storage->visit(tKey, &visitor);

	} catch (const ExceptionServer& e) {

		_return.errorCode = e.getError();
		_return.__set_errorMessage(e.getErrorMessage());

	} catch (...) {

		std::cerr << "Error unknown";
		ExceptionServer e = EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_UNKNOWN);
		_return.errorCode = e.getError();
		_return.__set_errorMessage(e.getErrorMessage());
	}
}

class getDoor_visitor : public PersistentStorageType::data_visitor {
public:

	getDoor_visitor(TResult& return_)
	: _return(return_) {
		// FIXME
		TRACE_FUNCTION;
	}

	virtual bool visit(const PersistentStorageType::TKey& key, PersistentStorageType::TValue& value) {
		if (value == model::ModelData::defVal) throw EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_ITEM_NOT_FOUND);
		auto &vl = _return.tValue;
		value.copyTo(vl);
		_return.errorCode = ErrorCode::ERR_NONE;
		_return.__isset.tValue = true;
		return false;
	}

private:
	TResult& _return;
};

void ZServiceModel::getDoor(TResult& _return, const TKey& tKey) {

	try {
		if (_storage.get() == NULL) throw EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_NULL_STORAGE);

		getDoor_visitor visitor(_return);
		_storage->visit(tKey, &visitor);

	} catch (const ExceptionServer& e) {

		_return.errorCode = e.getError();
		_return.__set_errorMessage(e.getErrorMessage());


	} catch (...) {
		std::cerr << "Error unknown";
		ExceptionServer e = EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_UNKNOWN);
		_return.errorCode = e.getError();
		_return.__set_errorMessage(e.getErrorMessage());

	}

}

class removeDoor_visitor : public PersistentStorageType::data_visitor {
public:

	removeDoor_visitor(TResult& return_)
	: _return(return_) {
		// FIXME
		TRACE_FUNCTION;
	}

	virtual bool visit(const PersistentStorageType::TKey& key, PersistentStorageType::TValue& value) {
		if (value == model::ModelData::defVal) throw EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_ITEM_NOT_FOUND);
		_return.errorCode = ErrorCode::ERR_NONE;
		return false;
	}

private:
	TResult& _return;
};

void ZServiceModel::removeDoor(TResult& _return, const TKey& tKey) {
	try {

		if (_storage.get() == NULL) throw EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_NULL_STORAGE);
		removeDoor_visitor visitor(_return);
		_storage->visit(tKey, &visitor);
		_storage->remove(tKey);

	} catch (const ExceptionServer& e) {
		_return.errorCode = e.getError();
		_return.__set_errorMessage(e.getErrorMessage());

	} catch (...) {
		std::cerr << "Error unknown";
		ExceptionServer e = EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_UNKNOWN);
		_return.errorCode = e.getError();
		_return.__set_errorMessage(e.getErrorMessage());

	}

}

class putDoor_visitor : public PersistentStorageType::data_visitor {
public:

	putDoor_visitor(TResult& return_, const TValue& vl)
	: _return(return_), _vl(vl) {
		// FIXME
		TRACE_FUNCTION;
	}

	virtual bool visit(const PersistentStorageType::TKey& key, PersistentStorageType::TValue& value) {
		value.copyFrom(_vl);
		_return.errorCode = ErrorCode::ERR_NONE;
		return true;
	}

private:
	TResult& _return;
	const Door::TValue& _vl;
};

void ZServiceModel::putDoor(TResult& _return, const TKey& tKey, const TValue & tValue) {

	try {

		if (_storage.get() == NULL) throw EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_NULL_STORAGE);
		putDoor_visitor putVisitor(_return, tValue);
		_storage->visit(tKey, &putVisitor);

	} catch (const ExceptionServer& e) {
		_return.errorCode = e.getError();
		_return.__set_errorMessage(e.getErrorMessage());

	} catch (...) {
		std::cerr << "Error unknown";
		ExceptionServer e = EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_UNKNOWN);
		_return.errorCode = e.getError();
		_return.__set_errorMessage(e.getErrorMessage());

	}
}

class updateDoor_visitor : public PersistentStorageType::data_visitor {
public:

	updateDoor_visitor(TResult& return_, const TValue& vl)
	: _return(return_), _vl(vl) {
		// FIXME
		TRACE_FUNCTION;
	}

	virtual bool visit(const PersistentStorageType::TKey& key, PersistentStorageType::TValue& value) {

		if (value == model::ModelData::defVal) throw EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_ITEM_NOT_FOUND);
		value.copyFrom(_vl);
		_return.errorCode = ErrorCode::ERR_NONE;

	}

private:
	TResult& _return;
	const Door::TValue& _vl;
};

void ZServiceModel::updateDoor(TResult& _return, const TKey& tKey, const TValue & tValue) {

	try {

		if (_storage.get() == NULL) throw EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_NULL_STORAGE);

		updateDoor_visitor visitor(_return, tValue);
		_storage->visit(tKey, &visitor);

	} catch (const ExceptionServer& e) {

		_return.errorCode = e.getError();
		_return.__set_errorMessage(e.getErrorMessage());

	} catch (...) {

		std::cerr << "Error unknown";
		ExceptionServer e = EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_UNKNOWN);
		_return.errorCode = e.getError();
		_return.__set_errorMessage(e.getErrorMessage());

	}
}

void ZServiceModel::listDoor(listResult& _return, const int32_t start, const int32_t limit) {
	try {

		auto kvStorage = ServiceFactory::getKVStorage()->getStorage("leveldb");
		auto iter = kvStorage->iterate();

		/* check list empty */
		std::string aKey;
		std::string aVal;
		if (!iter->next(aKey, aVal)) {
			_return.errorCode = ErrorCode::ERR_NONE;
			_return.__set_total(0);
			return;
		}


		/* check start out of range */
		int total = 1;
		while (total <= start) {
			if (!iter->next(aKey, aVal)) {
				ExceptionServer e = EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_OUT_OF_RANGE);
				_return.errorCode = e.getError();
				_return.errorMessage = e.getErrorMessage();
				return;
			}
			++total;
		}

		/* get first element */
		std::map<TKey, TValue> mapResult;
		TValue tValue;
		if (TThriftSerializer<TValue>::DeserializeT(tValue, aVal)) throw EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_UNKNOWN);
		mapResult[aKey] = tValue;


		/* get list element */
		for (int i = 0; i < limit - 1; ++i) {
			if (!iter->next(aKey, aVal)) break;
			TValue tValue;
			if (TThriftSerializer<TValue>::DeserializeT(tValue, aVal)) throw EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_UNKNOWN);
			mapResult[aKey] = tValue;

			++total;
		}

		/* get total */
		while (iter->next(aKey, aVal)) {
			++total;
		}

		_return.errorCode = ErrorCode::ERR_NONE;
		_return.__set_total(total);
		_return.__set_mapResult(mapResult);

	} catch (const ExceptionServer& e) {
		_return.errorCode = e.getError();
		_return.__set_errorMessage(e.getErrorMessage());

	} catch (...) {
		std::cerr << "Error unknown";
		ExceptionServer e = EXCEPTION_SERVER_TYPE(M2M_DOOR, ErrorCode::ERR_UNKNOWN);
		_return.errorCode = e.getError();
		_return.__set_errorMessage(e.getErrorMessage());

	}
}

void ZServiceModel::multiGetDoor(listResult& _return, const TKeyList& tKeyList) {

	_return.errorCode = ErrorCode::ERR_NONE;
	for (TKeyList::const_iterator iter = tKeyList.begin(); iter != tKeyList.end(); ++iter) {
		auto &mapValue = _return.mapResult;
		auto &tKey = *iter;
		Up::Core::M2M::Door::TResult result;
		getDoor(result, tKey);
		mapValue[tKey] = result.tValue;
	}
	if (!_return.mapResult.empty()) _return.__isset.mapResult = true;
}





}
}
}
}


