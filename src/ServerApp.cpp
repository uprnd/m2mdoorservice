/*
 * File:   ServerApp.cpp
 * Author: tiennd
 *
 * Created on November 7, 2014, 4:16 PM
 */

#include "ServerApp.h"
#include "Poco/Util/HelpFormatter.h"
#include "ServiceFactory.h"
#include "ServiceHandler.h"

#include "monitor/TStorageStatModule.h"
#include "monitor/TStorageMonitorThriftHandler.h"
#include "thriftutil/ClientManagerT.h"
#include "thriftutil/TClient.h"

using namespace Up::Core::M2M::Door;

ServerApp::ServerApp() : _showHelp(false) {
}

ServerApp::~ServerApp() {
}

void ServerApp::defineOptions(Poco::Util::OptionSet& options) {
	Poco::Util::ServerApplication::defineOptions(options);

	options.addOption(Poco::Util::Option("help", "help")
			.description("show help messages")
			.callback(Poco::Util::OptionCallback<ServerApp > (this, &ServerApp::handleShowHelp))
			);
}

void ServerApp::handleShowHelp(const std::string& name, const std::string& value) {
	_showHelp = true;
	Poco::Util::HelpFormatter helpFormatter(options());
	helpFormatter.setCommand(commandName());

	helpFormatter.setUsage("[OPTIONS]");
	helpFormatter.setHeader("Parameters: ");
	helpFormatter.format(std::cout);

}

void ServerApp::initialize(Poco::Util::Application& app) {
	if (_showHelp) return;

	loadConfiguration();
	ServiceFactory::init(app);

	ServiceHandler::TServiceThriftServer *aBizServer = new ServiceHandler::TServiceThriftServer(
			ServiceFactory::_svrPort
			, ServiceFactory::_workerCount
			, boost::shared_ptr< ServiceHandler > (new ServiceHandler(ServiceFactory::getModel())));

	this->addSubsystem(aBizServer);
	this->addSubsystem(new TStorageStatModule);

	TStorageMonitorThriftHandler* aStatHandler = new TStorageMonitorThriftHandler;

	this->addSubsystem(new TStorageMonitorThriftHandler::TStorageMonitorServer(
			ServiceFactory::_cfgPort
			, 2
			, boost::shared_ptr< up::up101::storage::monitor::StorageMonitorServiceIf > (aStatHandler)
			));

	aStatHandler->setCacheStorage(ServiceFactory::getCache().get(), ServiceFactory::getStorage().get(), ServiceFactory::getKVStorage().get());

	Poco::Util::ServerApplication::initialize(app);

	TStorageStatModule::getInstance().setStatusFetcher(ServiceFactory::getStatFetcher());
	ServiceFactory::getStorage()->setObserver(&TStorageStatModule::getInstance());
	ServiceFactory::getStorage()->setNumSavingThread(app.config().getInt("sns.cachepersistent.savingthread", 1));
	aStatHandler->setServer(aBizServer->thriftServer());

}

int ServerApp::main(const std::vector<std::string>& args) {
	if (config().hasOption("app.showhelp")) {
		return Application::EXIT_OK;
	}
	Poco::Util::ServerApplication::main(args);
	try {
		this->_zkReg.setZkHosts(ServiceFactory::_zkServers);
		if (this->_zkReg.registerActiveService(ServiceFactory::_zkRegPath, ServiceFactory::_svrHost, ServiceFactory::_svrPort, ServiceFactory::_zkScheme)) {
			logger().information("Registered with zookeeper");
		}
		this->_zkMonitorReg.setZkHosts(ServiceFactory::_zkServers);
		if (this->_zkMonitorReg.registerActiveService("/up-monitor/" + ServiceFactory::_zkRegPath, ServiceFactory::_svrHost, ServiceFactory::_cfgPort, "thrift_binary")) {
			logger().information("Registered monitor with zookeeper");
		}
	} catch (...) {

	}

	if (!_showHelp) waitForTerminationRequest();

	//flush db
	ServiceFactory::getKVStorage()->flush();
	return 0;
}

namespace Up {
namespace Caching {
Def_CaceFactory_FromString(model::ModelData, HasherType)
}
}