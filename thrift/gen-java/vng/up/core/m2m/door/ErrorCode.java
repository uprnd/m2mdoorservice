/**
 * Autogenerated by Thrift Compiler (0.9.1)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package vng.up.core.m2m.door;


import java.util.Map;
import java.util.HashMap;
import org.apache.thrift.TEnum;

public enum ErrorCode implements org.apache.thrift.TEnum {
  ERR_NONE(0),
  ERR_CONNECTION(1),
  ERR_ITEM_EXIST(2),
  ERR_ITEM_NOT_FOUND(3),
  ERR_OUT_OF_RANGE(4),
  ERR_NULL_STORAGE(5),
  ERR_UNKNOWN(6);

  private final int value;

  private ErrorCode(int value) {
    this.value = value;
  }

  /**
   * Get the integer value of this enum value, as defined in the Thrift IDL.
   */
  public int getValue() {
    return value;
  }

  /**
   * Find a the enum type by its integer value, as defined in the Thrift IDL.
   * @return null if the value is not found.
   */
  public static ErrorCode findByValue(int value) { 
    switch (value) {
      case 0:
        return ERR_NONE;
      case 1:
        return ERR_CONNECTION;
      case 2:
        return ERR_ITEM_EXIST;
      case 3:
        return ERR_ITEM_NOT_FOUND;
      case 4:
        return ERR_OUT_OF_RANGE;
      case 5:
        return ERR_NULL_STORAGE;
      case 6:
        return ERR_UNKNOWN;
      default:
        return null;
    }
  }
}
