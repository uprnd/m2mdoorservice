namespace java vng.up.core.m2m.door
namespace cpp Up.Core.M2M.Door

typedef string TKey
typedef list<string> TKeyList


enum ErrorCode
{
    ERR_NONE = 0,
    ERR_CONNECTION,
    ERR_ITEM_EXIST,
    ERR_ITEM_NOT_FOUND,
    ERR_OUT_OF_RANGE,
    ERR_NULL_STORAGE,
    ERR_UNKNOWN
}


struct TValue
{
	1: string name;
	2: string description;
	3: i16 id,
	4: i32 createTime,
	5: i32 lastUpdate
}



struct TResult
{
	1: i32 errorCode;
	2: optional string errorMessage;	
	3: optional TValue tValue;
}


struct listResult
{
	1: i32 errorCode;
	2: optional string errorMessage;
	3: optional map<TKey,TValue> mapResult;
	4: optional i32 total;
}
	

service M2MDoorService
{
	TResult addDoor(1: TKey tKey, 2: TValue tValue),	
	TResult putDoor(1: TKey tKey, 2: TValue tValue),
	TResult updateDoor(1: TKey tKey, 2: TValue tValue),
	TResult getDoor(1: TKey tKey),
	listResult multiGetDoor(1: TKeyList tKeyList),
	TResult removeDoor(1:TKey tKey),

	listResult listDoor(1: i32 start, 2: i32 limit )
}
